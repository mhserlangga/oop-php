<?php 
    require_once("Animal.php");
    class Frog extends Animal{
        public $cold_blooded = "Yes";

        public function jump(){
            echo "Jump : Hop Hop";
        }
    }

?>