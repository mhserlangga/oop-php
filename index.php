<?php 

require_once("Animal.php");
require_once("Frog.php");
require_once("Ape.php");

    $sheep = new Animal("Shaun the Sheep");
    echo "Name : " . $sheep->name . "<br>";
    echo "Legs : " . $sheep->legs . "<br>";
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>";

    $frog = new Frog("Pangeran Kodok");
    echo "Name : " . $frog->name . "<br>";
    echo "Legs : " . $frog->legs . "<br>";
    echo "Cold Blooded : " . $frog->cold_blooded . "<br>";
    $frog->jump();
    echo "<br><br>";

    $ape = new Ape("Lutung Kasarung");
    echo "Name : " . $ape->name . "<br>";
    echo "Legs : " . $ape->legs . "<br>";
    echo "Cold Blooded : " . $ape->cold_blooded . "<br>";
    $ape->yell();
?>